# How to Run 

## 1. Fork / Clone repo
- fork to your personal repo 
- clone to you local machine


## 2. Use a virtual environment

Сreate and activate virtual environment
```bash
python3 -m venv venv
echo "export PYTHONPATH=$PWD" >> venv/bin/activate
source venv/bin/activate
```

Install dependencies
```bash
pip install --upgrade pip
pip install -r requirements.txt
```

Run Jupyter Notebook 
```bash
jupyter notebook
```

To deactivate virtual environment: 
```bash
deactivate 
```


## 3. Or run with Docker 

Create config/.env
```bash
GIT_CONFIG_USER_NAME=<git user>
GIT_CONFIG_EMAIL=<git email>
```
example:

```config/.env
GIT_CONFIG_USER_NAME=ivan.varlamov
GIT_CONFIG_EMAIL=xxxyxxx16@gmail.com
```

Build
```bash
docker-compose  --env-file config/.env build
```

Run 
```bash
docker-compose run --name container_name --rm -p 8888:8888 nlp_banki_ru
```

Open notebook:
- open http://localhost:8888 in browser;
